\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{uebblatt}[2013/03/29 LaTeX class]

\DeclareOption{entwurf}{\AtEndOfClass{
  \RequirePackage{draftwatermark}
  \definecolor{pink}{rgb}{0.95,0.9,0.95}
  \SetWatermarkText{\textsf{\textcolor{pink}{ENTWURF}}}
  \SetWatermarkScale{1}
}}

\ProcessOptions\relax

\LoadClass[a4paper,ngerman]{scrartcl}
\RequirePackage{etex}

\RequirePackage{ifxetex}
\ifxetex\else\RequirePackage[utf8]{inputenc}\fi
\RequirePackage[ngerman]{babel}
\RequirePackage{amsmath,amsthm,amssymb,amscd,bussproofs,stmaryrd,color,graphicx,environ,tabto,xspace}
\RequirePackage[all,2cell]{xy}
\RequirePackage{mathtools}
\RequirePackage{framed}
\RequirePackage[protrusion=true,expansion=true]{microtype}
\RequirePackage{multicol}
\RequirePackage{lmodern}

\RequirePackage{geometry}
\geometry{tmargin=2cm,bmargin=2cm,lmargin=3.1cm,rmargin=3.1cm}

\RequirePackage{hyperref}

\RequirePackage{tikz}
\usetikzlibrary{calc,shapes.callouts,shapes.arrows}
\newcommand{\hcancel}[5]{%
    \tikz[baseline=(tocancel.base)]{
        \node[inner sep=0pt,outer sep=0pt] (tocancel) {#1};
        \draw[red, line width=0.3mm] ($(tocancel.south west)+(#2,#3)$) -- ($(tocancel.north east)+(#4,#5)$);
    }%
}

\setlength\parskip{\medskipamount}
\setlength\parindent{0pt}

\newlength{\titleskip}
\setlength{\titleskip}{1.5em}
\newenvironment{paper}{%
  \pagestyle{empty}%
  University of Verona \hfill
  Winter term 2022/2023 \\
  Ingo Blechschmidt
  \medskip
}{\newpage}

\newcounter{sheetnumber}
\newenvironment{sheet}{\begin{paper}
  \refstepcounter{sheetnumber}
  \begin{center}
    \Large \textbf{Minicourse on Category Theory: \\ Exercise sheet \thesheetnumber} \\[1em]
  \end{center}
  \vspace{\titleskip}}{\end{paper}}

\renewcommand*\theenumi{\alph{enumi}}
\renewcommand*\theenumii{\arabic{enumii}}
\renewcommand{\labelenumi}{(\theenumi)}
\renewcommand{\labelenumii}{\theenumii.}

\newlength{\exerciseskip}
\setlength{\exerciseskip}{1.5em}
\newcounter{aufgabennummer}[sheetnumber]
\renewcommand{\theaufgabennummer}{\thesheetnumber.\arabic{aufgabennummer}}
\newenvironment{exercise}[1]{
  \refstepcounter{aufgabennummer}
  \textbf{Exercise \theaufgabennummer{}.} \emph{#1} \par
}{\par\vspace{\exerciseskip}}
\newenvironment{exercise*}[1]{
  \refstepcounter{aufgabennummer}
  \textbf{Exercise* \theaufgabennummer{}.} \emph{#1} \par
}{\vspace{\exerciseskip}}
\newenvironment{exerciseE}[1]{\begin{exercise}{#1}\begin{enumerate}}{\end{enumerate}\end{exercise}}

\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000

\newcommand{\C}{\mathcal{C}}

\input{macros}
