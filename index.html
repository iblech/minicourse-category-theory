<!DOCTYPE html>
<html lang="en"><head>
  <title>Minicourse on Category Theory</title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="initial-scale=1">

  <style>
    body {
      font-family: "Cascadia Code", monospace, sans-serif;
      margin-left: auto; margin-right: auto;
      max-width: 45em;
      padding: 0.2em;
    }

    a:not([class]) { text-decoration: none; background-image: linear-gradient(#eeebee 0%, #eeebee 100%); background-repeat: repeat-x; background-position: 0 100%; background-size: 1px 1px; }
    a:not([class]):hover { background-image: linear-gradient(rgba(114, 83, 237, 0.45) 0%, rgba(114, 83, 237, 0.45) 100%); background-size: 1px 1px; }

    * { line-height: 1.5em; box-sizing: border-box; }

    h1 { text-align: center; }

    p, pre { padding-left: 0.5em; padding-right: 0.5em; }

    footer, footer a { color: gray; }

    ol.wide > li:not(:last-child) {
       margin-bottom: 1em;
    }
    ol.wide > li::marker {
      font-weight: bold;
    }
  </style>
</head><body>

<a href="https://brownsharpie.courtneygibbons.org/comic/my-favorite-universal-property/"><img src="images/love-commute.jpeg" width="500" height="333" style="width: 50%; height: auto; aspect-ratio: attr(width) / attr(height); margin-left: auto; margin-right: auto; display: block"></a>

<h1>Minicourse on Category&nbsp;Theory</h1>
<p style="margin-top: -3em; text-align: center; font-size: small; font-weight:
normal"><br>at the University of Verona &bull; begin November 10th, 2022 &bull; by Ingo
Blechschmidt</p>

<p>Welcome to our joint course on category theory. 👋</p>

<p>The language of category theory has proved to be the lingua franca of
diverse subject areas including considerable parts of algebra, geometry,
topology, mathematical logic, theoretical computer science and mathematical
physics. The strength of category theory is that manifold phenomena can be seen
together. As a basic example, category theory reveals a common origin to the
commutativity of binary operations such as addition and multiplication of
numbers; the cartesian product of sets, groups and spaces; and the pair type in
programming. After a gentle introduction into the foundations of category
theory, this course will be dedicated to contemporary applications. The course
requires no particular prerequisites.</p>

<p>I hope you will enjoy the course. <em>Questions and suggestions are always welcome!</em></p>

<ul>
  <li><strong><a href="exercises.pdf">Exercise sheets</a></strong></li>
  <li><a href="tel:+4917695110311">Contact Ingo on Telegram</a></li>
</ul>

<h2>📆 Schedule</h2>

<pre>
    November 2022
Mo Tu We Th Fr Sa Su
    1  2  3  4  5  6
 7  8  9 <strong>10</strong> <strong>11</strong> 12 13
<strong>14</strong> 15 <strong>16</strong> <strong>17</strong> <strong>18</strong> 19 20
<strong>21</strong> 22 <strong>23</strong> <strong>24</strong> <strong>25</strong> 26 27
28 29 30

Mondays:    15:30–19:30, Aula B
Wednesdays: 17:30–19:30, Aula C
Thursdays:   9:30–10:30, Aula H
Fridays:    12:30–13:30, Aula C
</pre>

<p>
  <strong style="color: darkred">Tomorrow, on Friday Nov 24th, directly after
  our final category theory session, there is an extra session on free monads
  and freer monads in Haskell.</strong>
</p>

<h2>💡 Syllabus</h2>

<ol class="wide">
  <li><strong>Categories</strong>
    <ul>
      <li>Motivation for category theory</li>
      <li>Categories: definition and examples</li>
      <li>Initial and terminal objects: definition and examples</li>
      <li>Monomorphisms, epimorphisms and isomorphisms: definition and
      characterization in Set</li>
      <li>Opposite category: definition and applications</li>
    </ul></li>
  <li><strong>Products and coproducts</strong>
    <ul>
      <li>Definition of products (binary case)</li>
      <li>Examples in Set, Vect(ℝ), Pok</li>
      <li>Examples in Setᵒᵖ, Vect(ℝ)ᵒᵖ</li>
      <li>Definition of coproducts (binary case)</li>
      <li>Products and coproducts of empty families</li>
      <li>Products and coproducts of families in Vect(ℝ)</li>
    </ul></li>
  <li><strong>Functors</strong>
    <ul>
      <li>Definition</li>
      <li>Structural examples: identity, constant</li>
      <li>Examples from algebraic constructions</li>
      <li>Forgetful functors</li>
      <li>Powerset functors</li>
      <li>Functors between discrete categories as generalizations of maps between sets</li>
      <li>The 1-category of categories and functors involving this category</li>
      <li>Functors as diagrams</li>
      <li>Properties of functors: full, faithful, essentially surjective</li>
      <li>Elementary equivalences of categories</li>
    </ul></li>
  <li><strong>Natural transformations</strong>
    <ul>
      <li>Definition</li>
      <li>Motto: Natural transformations as uniform families of maps</li>
      <li>Examples involving sets and lists</li>
      <li>Examples from linear algebra</li>
      <li>Natural isomorphisms</li>
      <li>Horizontal and vertical composition</li>
      <li>Strong equivalences of categories</li>
    </ul></li>
  <li><strong>Limits</strong>
    <ul>
      <li>Motivating examples</li>
      <li>Definition of limits as terminal cones</li>
      <li>Colimits as the dual notion</li>
      <li>Examples in the category of topological spaces</li>
      <li>Products and terminal objects as special limits</li>
      <li>Equalizers, fiber products and pullback</li>
      <li>Complete categories</li>
      <li>Existence of limits and colimits in the category of Sets</li>
    </ul></li>
  <li><strong>The Yoneda lemma and adjoints</strong>
    <ul>
      <li>The real numbers as a completion of the rational numbers by ideal entities</li>
      <li>Presheaves as ideal objects</li>
      <li>Representable presheaves</li>
      <li>Statement of the Yoneda lemma</li>
      <li>The Yoneda embedding as a corollary</li>
      <li>Yoneda-style proofs</li>
      <li>The category of presheaves as a free cocompletion</li>
      <li>Definition of adjoint functor pairs</li>
      <li>Examples for adjoint functors</li>
      <li>Interaction with preservation of (co-)limits</li>
    </ul></li>
  <li><strong>Monads, monoidal categories and tensor categories</strong>
    <ul>
      <li>Diagrammatic definition of monoids</li>
      <li>Definition of monads</li>
      <li>Examples for monads</li>
      <li>Monads for effects</li>
      <li>Monads as basis-free algebraic theories</li>
      <li>Monads and adjunctions</li>
      <li>General monoidal categories</li>
      <li>Examples for monoidal categories</li>
      <li>Examples for monoid objects in monoidal categories</li>
    </ul></li>
  <li>Introduction to quantum field theory</li>
  <li>Applications III</li>
</ol>

<h3>Side topic: Sets and classes</h3>

<ol>
  <li>Motto and examples</li>
  <li>Russell's paradox and its resolution</li>
  <li>Proper classes in ZFC</li>
  <li>Dealing with size issues</li>
  <li>Other set-theoretic phantoms</li>
</ol>

<h3>Side topic: Agda, the programming language, proof language and proof assistant</h3>

<ol>
  <li>Basic functions with Booleans and natural numbers</li>
  <li>Polymorphic functions and dependent types</li>
  <li>Predicates: the Boolean and the witness-based approach</li>
  <li>Propositional equality</li>
  <li>Proofs with natural numbers</li>
  <li>Induction as recursion</li>
</ol>

<h2>📖 References</h2>

<ul>
  <li>Jiří Adámek, Horst Herrlich, George Strecker.
  <em><a href="http://www.tac.mta.ca/tac/reprints/articles/17/tr17.pdf">Abstract and
  Concrete Categories, The Joy of Cats</a></em> (pdf). Dover,
  2009.</li>
  <li>Steve Awodey. <em>Category Theory</em>. Clarendon Press, 2006.</li>
  <li>Michael Barr, Charles Wells. <em><a
  href="http://www.math.mcgill.ca/triples/Barr-Wells-ctcs.pdf">Category Theory for Computing
  Science</a></em> (pdf). Prentice Hall, 1990.</li>
  <li>Martin Brandenburg. <em>Einführung in die Kategorientheorie.
  Mit ausführlichen Erklärungen und zahlreichen Beispielen</em>. Springer,
  2016.</li>
  <li>William Lawvere. <em><a
  href="https://www.emis.de/journals/TAC/reprints/articles/8/tr8.pdf">Taking
  categories seriously</a></em> (pdf). Reprints in Theory and Applications
  of Categories, No. 8, 2005, pages 1&ndash;24.</li>
  <li>William Lawvere, Steve Schanuel. <em>Conceptual
  Mathematics: A
  First Introduction to Categories</em>. Cambridge University
  Press, 1997.</li>
  <li>Saunders Mac Lane. <em>Categories for the Working Mathematician</em>. Springer, 1971.</li>
  <li>Sergei Gelfand, Yuri Manin. <em>Methods
  of homological algebra</em>. Springer, 1996. (Beware of typos)</li>
  <li><strong>Emily Riehl. <em><a
  href="https://www.math.jhu.edu/~eriehl/context.pdf">Category theory in
  context</a></em> (pdf). Dover Publications, 2016.</strong></li>
  <li>Tom Leinster. <em><a
  href="https://www.maths.ed.ac.uk/~tl/msci/">Category Theory</a></em> (web),
  2007.</li>
  <li>Bartosz Milewski. <em><a href="https://bartoszmilewski.com/2014/10/28/category-theory-for-programmers-the-preface/">Category
  Theory for Programmers</a></em> (web), 2019.</li>
  <li>Jaap van Oosten. <em><a
  href="https://www.staff.science.uu.nl/~ooste110/syllabi/catsmoeder.pdf">Basic
  Category Theory</a></em> (pdf),
  2002.</li>
  <li>Emily Riehl. <em><a
  href="https://emilyriehl.github.io/files/survey.pdf">A survey of
  categorical concepts</a></em> (pdf).
  2012.</li>
  <li>David Spivak. <em><a
  href="https://ia600406.us.archive.org/6/items/cattheory/cattheory.pdf">Category theory for
  the sciences</a></em> (pdf), 2014.</li>
  <li>The Catsters. <em><a
  href="https://www.youtube.com/user/thecatsters">Various topics in category
  theory</a></em> (YouTube).
  2007&ndash;2011.</li>
  <li>John Baez. <em><a
  href="https://math.ucr.edu/home/baez/categories.html">Categories,
  Quantization, and Much More</a></em> (web), 2006.</li>
  <li>Jean-Pierre Marquis. <em><a
  href="https://plato.stanford.edu/entries/category-theory/">Category
  Theory</a></em> (web). The Stanford Encyclopedia
  of Philosophy, Edward N. Zalta (ed.), 2010.</li>
</ul>

<h2>👋 Side topics</h2>

<ul>
  <li><a href="https://arxiv.org/abs/2204.00948">Introduction to toposes</a></li>
  <li><a href="https://www.speicherleck.de/iblech/stuff/algar-lecture-notes.pdf">Introduction to constructive mathematics</a></li>
  <li><a href="https://plato.stanford.edu/entries/philosophy-mathematics/">Introduction to philosophy of mathematics</a> (further keywords: platonism, formalism, fictionalism)</li>
  <li><a href="https://www.janis-voigtlaender.eu/free-slides.pdf">Theorems for free</a></li>
  <li><a href="https://iblech.gitlab.io/bb/multiverse.html">Provability, incompleteness, continuum hypothesis, multiverse philosophy</a></li>
  <li>Agda:
    <a href="https://agdapad.quasicoherent.io/#testvr">transcript</a>,
    (<a href="https://agdapad.quasicoherent.io/~testvr/html/hello.html">browse</a>),
    <a href="https://agdapad.quasicoherent.io/~AgdaPadova">exercises</a>,
    <a href="https://hott.github.io/book/hott-online-1353-g16a4bfa.pdf#page=23">Rosetta stone</a>,
    <a href="https://agda.github.io/agda-stdlib/README.html">standard library</a>
  </li>
  <li><a href="https://www.lesswrong.com/posts/ALCnqX6Xx8bpFMZq3/the-cartoon-guide-to-loeb-s-theorem">A cartoon proof of Löb's theorem</a></li>
  <li><a href="https://www.youtube.com/watch?v=SXHHvoaSctc&amp;list=PLTBqohhFNBE_09L0i-lf3fYXF5woAbrzJ&amp;index=1">Introduction to geometry and topology</a></li>
  <li>Topological quantum field theory:
    <a href="https://stanford.edu/~sfh/tqftslides.pdf">slides</a>,
    <a href="https://math.ucr.edu/home/baez/quantum/quantum.pdf">thesis:
    oddities of quantum mechanics are unintuitive to us because Hilb is much
    more like Rel instead of Set</a>
  </li>
  <li>Free monads:
    <a href="https://agdapad.quasicoherent.io/~testvr/Free.hs">transcript</a>,
    <a href="https://okmij.org/ftp/Haskell/extensible/more.pdf">paper by Oleg</a>,
    <a href="https://github.com/atnos-org/eff">Haskell library</a>,
    <a href="https://curry-club-augsburg.de/files/effektsysteme.pdf">slides</a>
  </li>
</ul>
</ul>

<h2>💬 Contact</h2>

<p>mail: <a href="mailto:iblech@speicherleck.de">iblech@speicherleck.de</a><br>
phone: <a href="tel:+4917695110311">+49 176 95110311</a> (also Telegram)</p>

<footer>
  Spot any mistakes? The source is
  <a href="https://gitlab.com/iblech/minicourse-category-theory">on GitLab</a>!
</footer>

</body>
</html>
